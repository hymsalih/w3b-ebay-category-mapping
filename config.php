<?php

$customer_id = 1;
global $pm;
$store = $pm->fetchResult("SELECT * FROM ebay_store WHERE customer_id=$customer_id");
if (empty($store)) {
    echo "ebay store api access not found\n";
    die;
}
$developer_id = (!empty($store) ? $store[0]['developer_id'] : '');
$application_id = (!empty($store) ? $store[0]['application_id'] : '');
$certificate_id = (!empty($store) ? $store[0]['certificate_id'] : '');
$compability_level = (!empty($store) ? $store[0]['compability_level'] : '');
$site_id = (!empty($store) ? $store[0]['site_id'] : '');
$token = (!empty($store) ? $store[0]['token'] : '');
$api_url = "https://api.ebay.com/ws/api.dll";
$country = "US";
$currency = "USD";
$postal_code = "95125";
