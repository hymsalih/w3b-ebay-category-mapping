<?php
require_once "db/DatabaseManager.php";
$pm = new DatabaseManager();
$categories = $pm->fetchResult("SELECT * FROM `tbl_ebay_categories` WHERE category_level = 1 ORDER BY category_name ASC");
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <style>
        .btn {
            color: #fff !important;
        }
    </style>
</head>
<body>
<div class="container">

    <!doctype html>
    <link rel="dns-prefetch" href="//ir.ebaystatic.com">
    <link rel="dns-prefetch" href="//secureir.ebaystatic.com">
    <link rel="dns-prefetch" href="//i.ebayimg.com">
    <link rel="dns-prefetch" href="//rover.ebay.com">
    <script>$ssgST = new Date().getTime();</script>
    <style>
        /*! normalize.css v5.0.0 | MIT License | github.com/necolas/normalize.css */
        html {
            font-family: sans-serif;
            line-height: 1.15;
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%
        }

        body {
            margin: 0
        }

        article, aside, footer, header, nav, section {
            display: block
        }

        h1 {
            font-size: 2em;
            margin: .67em 0
        }

        figcaption, figure, main {
            display: block
        }

        figure {
            margin: 1em 40px
        }

        hr {
            box-sizing: content-box;
            height: 0;
            overflow: visible
        }

        pre {
            font-family: monospace, monospace;
            font-size: 1em
        }

        a {
            background-color: transparent;
            -webkit-text-decoration-skip: objects
        }

        a:active, a:hover {
            outline-width: 0
        }

        abbr[title] {
            border-bottom: 0;
            text-decoration: underline;
            text-decoration: underline dotted
        }

        b, strong {
            font-weight: inherit
        }

        b, strong {
            font-weight: bolder
        }

        code, kbd, samp {
            font-family: monospace, monospace;
            font-size: 1em
        }

        dfn {
            font-style: italic
        }

        mark {
            background-color: #ff0;
            color: #000
        }

        small {
            font-size: 80%
        }

        sub, sup {
            font-size: 75%;
            line-height: 0;
            position: relative;
            vertical-align: baseline
        }

        sub {
            bottom: -0.25em
        }

        sup {
            top: -0.5em
        }

        audio, video {
            display: inline-block
        }

        audio:not([controls]) {
            display: none;
            height: 0
        }

        img {
            border-style: none
        }

        svg:not(:root) {
            overflow: hidden
        }

        button, input, optgroup, select, textarea {
            font-family: sans-serif;
            font-size: 100%;
            line-height: 1.15;
            margin: 0
        }

        button, input {
            overflow: visible
        }

        button, select {
            text-transform: none
        }

        button, html [type="button"], [type="reset"], [type="submit"] {
            -webkit-appearance: button
        }

        button::-moz-focus-inner, [type="button"]::-moz-focus-inner, [type="reset"]::-moz-focus-inner, [type="submit"]::-moz-focus-inner {
            border-style: none;
            padding: 0
        }

        button:-moz-focusring, [type="button"]:-moz-focusring, [type="reset"]:-moz-focusring, [type="submit"]:-moz-focusring {
            outline: 1px dotted ButtonText
        }

        fieldset {
            border: 1px solid silver;
            margin: 0 2px;
            padding: .35em .625em .75em
        }

        legend {
            box-sizing: border-box;
            color: inherit;
            display: table;
            max-width: 100%;
            padding: 0;
            white-space: normal
        }

        progress {
            display: inline-block;
            vertical-align: baseline
        }

        textarea {
            overflow: auto
        }

        [type="checkbox"], [type="radio"] {
            box-sizing: border-box;
            padding: 0
        }

        [type="number"]::-webkit-inner-spin-button, [type="number"]::-webkit-outer-spin-button {
            height: auto
        }

        [type="search"] {
            -webkit-appearance: textfield;
            outline-offset: -2px
        }

        [type="search"]::-webkit-search-cancel-button, [type="search"]::-webkit-search-decoration {
            -webkit-appearance: none
        }

        ::-webkit-file-upload-button {
            -webkit-appearance: button;
            font: inherit
        }

        details, menu {
            display: block
        }

        summary {
            display: list-item
        }

        canvas {
            display: inline-block
        }

        template {
            display: none
        }

        [hidden] {
            display: none
        }

        html {
            line-height: normal
        }

        button, input, optgroup, select, textarea {
            font-family: "Helvetica Neue", Helvetica, Arial, Roboto, sans-serif
        }

        a, a:focus, a:visited {
            color: #0654ba
        }

        .hide {
            display: none
        }

        .show {
            display: block
        }

        .left {
            float: left
        }

        .right {
            float: right
        }

        .center {
            margin: 0 auto
        }

        .emphasis {
            color: #dd1e31
        }

        .positive {
            color: #447d14
        }

        .negative {
            color: #dd1e31
        }

        .no-scroll {
            overflow: hidden
        }

        .fluid {
            width: 100%
        }

        .no-wrap {
            white-space: nowrap
        }

        .clearfix::before, .clearfix::after {
            display: table;
            content: " ";
            line-height: 0
        }

        .clearfix::after {
            clear: both
        }

        .image-wrapper {
            display: table-cell;
            text-align: center;
            vertical-align: middle
        }

        .image-wrapper img {
            max-height: 100%;
            max-width: 100%
        }

        .clipped {
            border: 0;
            clip: rect(1px, 1px, 1px, 1px);
            -webkit-clip-path: inset(50%);
            clip-path: inset(50%);
            height: 1px;
            overflow: hidden;
            padding: 0;
            position: absolute;
            white-space: nowrap;
            width: 1px
        }

        .clipped--stealth:focus {
            clip: auto;
            -webkit-clip-path: none;
            clip-path: none;
            height: auto;
            overflow: visible;
            white-space: normal;
            width: auto
        }

        .truncate {
            overflow: hidden;
            white-space: nowrap;
            text-overflow: ellipsis
        }

        .responsive-image {
            height: auto;
            max-width: 100%
        }

        .fluid-image {
            height: auto;
            width: 100%
        }

        .emphasis {
            color: #dd1e31
        }

        .positive {
            color: #447d14
        }

        .negative {
            color: #dd1e31
        }

        .no-scroll {
            overflow: hidden
        }

        .fluid {
            width: 100%
        }

        .no-wrap {
            white-space: nowrap
        }

        .view-window {
            height: 225px;
            width: 1890px;
            overflow: hidden
        }

        .caty-browse-container {
            height: 225px;
            width: 285px;
            left: 0;
            position: relative
        }

        .caty-browse-container .caty-list-container ul {
            border: 1px solid #eee;
            box-sizing: border-box;
            cursor: pointer;
            float: left;
            height: 225px;
            list-style: none;
            margin: 0;
            padding: 6px 0;
            outline: 0;
            overflow: scroll;
            width: 285px
        }

        .caty-browse-container .caty-list-container ul:focus {
            border-color: #999
        }

        .caty-browse-container .caty-list-container li {
            cursor: pointer;
            font-size: .8125rem;
            line-height: 1;
            padding: 6px 8px
        }

        .caty-browse-container .caty-list-container li[selected] {
            background-color: #eee
        }

        .caty-browse-container .caty-list-spacer {
            cursor: pointer;
            float: left;
            font-size: 26px;
            height: 225px;
            line-height: 225px;
            text-align: center;
            vertical-align: middle;
            width: 30px
        }
    </style>


    <h1>w3bstore to eBay category Mapping</h1>
    <br>
    <div class="form-group">
        <label for="exampleFormControlTextarea1">w3bstore Store Category Name </label>
        <input type="text" required id="w3bstore_cat_name" class="form-control">
        <input type="hidden" required id="category_id" class="form-control">
    </div>
    <div class="form-group">
        <label for="exampleFormControlTextarea1">eBay Store Category Name</label>
    </div>
    <div id="view-window" class="view-window">
        <div class="caty-browse-container" id="caty-browse-container" data-w-onclick="checkAction|caty-browse-container"
             data-w-onkeydown="keyNavigation|caty-browse-container"
             data-widget="/febear$22.0.0/src/components/category-browser/widget">
            <div class="caty-list-container" id="container_1">
                <ul size="8" id="caty_1" tabindex="0">
                    <?php
                    foreach ($categories as $categorie) {
                        ?>
                        <li role="option" class=" " value="<?php echo $categorie["category_id"] ?>"
                            action="updateSelected"
                            tabindex="-1"><?php echo $categorie["category_name"] ?></li>
                        <?php
                    }
                    ?>
                </ul>
            </div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-2 pull=left">
            <div class="form-group">
                <button class="btn btn-sm btn-success btn-block save">Save</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="ebay_cat_template.js"></script>
<script>$_mod.ready();</script>
<noscript id="markoWidgets" data-ids="caty-browse-container"></noscript>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
<script>
    $(".save").click(function () {
        //!= "" && $("#w3bstore_cat_name").val() != ""
        if ($("#w3bstore_cat_name").val() == "") {
            alert("Please enter w3bstore store category name");
        } else if ($("#category_id").val() == "") {
            alert("Please select ebay category up to leaf category");
        } else {
            var data = "ebay_cat=" + $("#category_id").val() + "&w3bstore_cat_name=" + $("#w3bstore_cat_name").val();
            $.ajax({
                type: "POST",
                url: "w3bstore_cat_to_ebay_cat.php",
                data: data,
                success: function (data) {
                    alert("success");
                    // window.location = "";
                },
            });
        }
    })
</script>
</body>
</html>