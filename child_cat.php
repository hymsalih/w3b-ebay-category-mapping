<?php
/**
 * Created by PhpStorm.
 * User: Salih Mohamed
 * Date: 10/2/2018
 * Time: 11:16 PM
 */
require_once "db/DatabaseManager.php";
$db = new DatabaseManager();
$sql = "SELECT * FROM tbl_ebay_categories WHERE category_parent_id='" . $_REQUEST['categoryId'] . "' AND category_level='" . $_REQUEST['level'] . "' order by category_name asc";
$categories = $db->fetchResult($sql);
$tml = "";
foreach ($categories as $category) {
    $sql = "SELECT * FROM tbl_ebay_categories WHERE category_parent_id='" . $category['category_id'] . "'";
    $sub_categories = $db->fetchResult($sql);
    if (count($sub_categories) > 0) {
        $tml .= '<li role="option" class=" " value="' . $category['category_id'] . '" action="updateSelected"
                    tabindex="-1">' . $category["category_name"] . '</li>';
    } else {
        $tml .= '<li role="option" class="leaf "  value="' . $category['category_id'] . '" action="updateSelected"
                    tabindex="-1">' . $category["category_name"] . '</li>';
    }
}
echo $tml;